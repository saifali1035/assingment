# test_database.py

import unittest
from database import Database

class TestDatabase(unittest.TestCase):
    def setUp(self):
        self.db = Database()

    def test_insert_and_get_all(self):
        data = {'name': 'John Doe', 'age': '30', 'email': 'john@example.com', 'phone': '1234567890'}
        self.db.insert(data)
        all_data = self.db.get_all()
        self.assertEqual(len(all_data), 1)
        self.assertEqual(all_data[0], data)

if __name__ == '__main__':
    unittest.main()

