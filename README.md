# GitLab CI/CD Pipeline

## Overview
This GitLab CI/CD pipeline automates the deployment process of a Python application to Amazon ECS (Elastic Container Service). The pipeline consists of four stages: cloning the repository, testing the application, dockerizing the application, and deploying it to ECS.

---

## Stages

### Stage: Clone Repository
This stage clones the repository into the GitLab Runner's workspace.

- **Job**: `clone_repo`
- **Script**:
  - `git clone $CI_REPOSITORY_URL`: Clone the repository specified by the environment variable `$CI_REPOSITORY_URL`.
- **Rules**:
  - `if: $CI_PIPELINE_SOURCE == "push"`: Run only when triggered by a push event.
- **Tags**:
  - `ubuntu`: Run on a runner with the tag "ubuntu".

### Stage: Test App
This stage runs unit tests for the backend application.

- **Job**: `test_application`
- **Script**:
  - `python3 -m unittest discover -v`: Execute unit tests for the backend application.
- **Tags**:
  - `ubuntu`: Run on a runner with the tag "ubuntu".
- **Dependencies**:
  - `clone_repo`: Depend on the successful completion of the `clone_repo` job.

### Stage: Dockerize
This stage builds a Docker image for the application and pushes it to Amazon ECR.

- **Job**: `docker_build_and_push_to_ecr`
- **Image**:
  - `bentolor/docker-dind-awscli`: Docker image used to execute this job.
- **Services**:
  - `docker:dind`: Docker service within the job container.
- **Variables**:
  - `DOCKER_TLS_CERTDIR`: Ensure Docker uses default TLS configuration.
  - `TAG`: Store the Git commit short SHA.
- **Script**:
  - `docker build -t my-python-app .`: Build the Docker image.
  - `aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com`: Login to the ECR.
  - `docker tag my-python-app:latest $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/my-python-app:$TAG`: Tag the Docker image.
  - `docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/my-python-app:$TAG`: Push the Docker image to ECR.
- **Tags**:
  - `ubuntu`: Run on a runner with the tag "ubuntu".
- **Dependencies**:
  - `clone_repo`: Depend on the successful completion of the `clone_repo` job.

### Stage: Deploy to ECS
This stage deploys the application to Amazon ECS.

- **Job**: `deploy_to_ecs`
- **Image**:
  - `python:3.8`: Docker image used to execute this job.
- **Script**:
  - `ECS_CLUSTER_NAME=$(aws ecs list-clusters | grep -oP '(?<=cluster/)[^"]+')`: Fetch the ECS cluster name.
  - `ECS_SERVICE_NAME=$(aws ecs list-services --cluster $ECS_CLUSTER_NAME | grep -oP '(?<=service/)[^"]+')`: Fetch the ECS service name.
  - `IMAGE_TAG=$(aws ecr describe-images --repository-name my-python-app --region $AWS_REGION --query 'sort_by(imageDetails,& imagePushedAt)[-1].imageTags[0]' --output text)`: Retrieve the latest image tag from ECR.
  - `REPOSITORY_URI=$(aws ecr describe-repositories --repository-names my-python-app --region $AWS_REGION --query 'repositories[0].repositoryUri' --output text)`: Get the repository URI from ECR.
  - `FULL_IMAGE_URI="$REPOSITORY_URI:$IMAGE_TAG"`: Construct the full image URI.
  - `TASK_DEF_ARN=$(aws ecs describe-services --cluster $ECS_CLUSTER_NAME --services $ECS_SERVICE_NAME --query 'services[0].taskDefinition' --output text)`: Retrieve the task definition ARN.
  - `TASK_DEF_JSON=$(aws ecs describe-task-definition --task-definition $TASK_DEF_ARN --query 'taskDefinition' --output json)`: Describe the task definition JSON.
  - `UPDATED_TASK_DEF_JSON=$(echo $TASK_DEF_JSON | jq --arg FULL_IMAGE_URI $FULL_IMAGE_URI '.containerDefinitions[0].image = $FULL_IMAGE_URI' | jq 'del(.taskDefinitionArn, .revision, .status, .requiresAttributes, .compatibilities, .registeredAt, .registeredBy)')`: Update the container image URI in the task definition JSON.
  - `NEW_TASK_DEF_ARN=$(aws ecs register-task-definition --cli-input-json "$UPDATED_TASK_DEF_JSON" --query 'taskDefinition.taskDefinitionArn' --output text)`: Register the updated task definition.
  - `aws ecs update-service --cluster $ECS_CLUSTER_NAME --service $ECS_SERVICE_NAME --task-definition $NEW_TASK_DEF_ARN --force-new-deployment --region $AWS_REGION`: Update the ECS service with the new task definition.
- **Only**:
  - `main`: Run only when changes are pushed to the main branch.
- **Tags**:
  - `ubuntu`: Run on a runner with the tag "ubuntu".
- **Dependencies**:
  - `docker_build_and_push_to_ecr`: Depend on the successful completion of the `docker_build_and_push_to_ecr` job.

