# test_backend.py

import unittest
from backend import process_data

class TestBackend(unittest.TestCase):
    def test_process_data(self):
        data = 'Test Data'
        processed_data = process_data(data)
        self.assertEqual(data, processed_data)

if __name__ == '__main__':
    unittest.main()

