# Use the official Python image as a base image
FROM python:3.9-slim

# Set the working directory in the container
WORKDIR /app

# Copy the requirements file into the container
COPY requirements.txt .

# Install Python dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Copy the application code into the container
COPY . .

# Expose port 5000 to the outside world
EXPOSE 5000

ENV FLASK_APP=frontend.py

# Install compatible versions of Flask-SQLAlchemy and SQLAlchemy
RUN pip install --no-cache-dir Flask-SQLAlchemy==3.1.1 SQLAlchemy==2.0.29

# Command to run the Flask application
CMD ["python3","-m", "flask", "run", "--host", "0.0.0.0"]

