# test_frontend.py

import unittest
from frontend import app, db, User

class TestApp(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.client = app.test_client()

        with app.app_context():
            db.create_all()

    def tearDown(self):
        with app.app_context():
            db.session.remove()
            db.drop_all()

    def test_home_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Enter User Data', response.data)

    def test_submit_form(self):
        response = self.client.post('/', data=dict(
            name='Test User',
            age='30',
            email='test@example.com',
            phone='1234567890'
        ), follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Test User', response.data)
        self.assertIn(b'30', response.data)
        self.assertIn(b'test@example.com', response.data)
        self.assertIn(b'1234567890', response.data)

    def test_details_page(self):
        response = self.client.get('/details')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'User Details', response.data)

if __name__ == '__main__':
    unittest.main()
